# mastodon-reminder-node

Sends an e-mail alert from a template in pug format with a list of users in JSON format.

## Getting started

First you must export the list of users to whom you need to send the notice. For example, if you want to notify users who have not logged in for less than 6 months since a certain date.

We must connect to the database with DBeaber or some similar program and make the following query:

```
select email, last_sign_in_at, accounts.username
from users, accounts
where email not like '%xarxa.cloud%'
and last_sign_in_at <= '2022-04-01'
and accounts.id = users.account_id
```

When you have the list, you can export the result in JSON format. 

The JSON file should contain data in the following form:

```
[
        {
                "email" : "xxxx@XXXXX",
                "last_sign_in_at" : "2022-08-30T20:24:22.965Z",
                "username" : "XXXXXX"
        },
        {
                "email" : "xxxxxx@XXXXXX",
                "last_sign_in_at" : "2022-04-30T10:23:51.256Z",
                "username" : "XXXXXX"
        } 
]
```

