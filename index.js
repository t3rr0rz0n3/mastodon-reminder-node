const nodemailer = require('nodemailer');
const pug = require('pug');
const fs = require('fs');
const moment = require('moment');
require('dotenv').config()

// Cargamos la plantilla en formato pug
const template = pug.compileFile('template.pug');
// Leemos el contenido del archivo de la plantilla
const emailContent = template({ name: 'Usuario', account: "Cuenta", date: 'Fecha' });
// Leemos la lista de usuarios desde el archivo JSON
const userList = JSON.parse(fs.readFileSync('usuarios.json'));

// Configuración del transporter del correo
const transporter = nodemailer.createTransport({
  host: process.env.HOST,
  port: process.env.PORT,
  secure: false,
  service: 'smtp',
  auth: {
    user: process.env.AUTH_USER,
    pass: process.env.AUTH_PASS
  },
  tls: {
    rejectUnauthorized: false
  }
});

// Recorremos todos los usuarios y enviamos el correo
userList.forEach((user, index) => {
  setTimeout(() => {
    let personalizedContent = emailContent;
    personalizedContent = personalizedContent.replace('Usuario', user.username);
    personalizedContent = personalizedContent.replace('Cuenta', user.username);
    personalizedContent = personalizedContent.replace('Fecha', moment(user.last_sign_in_at).format('DD-MM-YYYY'));

    const mailOptions = {
      from: process.env.FROM,
      to: user.email,
      subject: process.env.SUBJECT,
      html: personalizedContent,
      headers: {
        'Content-Type': 'text/html; charset=utf-8',
      },
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log(`Error al enviar el correo a ${user.email}: ${error}`);
      } else {
        console.log(`Correo enviado a ${user.email}: ${info.response}`);
      }
    });
  }, index * 1000);
});
